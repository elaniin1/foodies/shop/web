# :hamburger: Foodies

## Running the project

## Clone the project

```
git clone https://gitlab.com/elaniin1/foodies/shop/web.git
```

## Install dependencies

```
npm install
```

## To run the project locally you need a .env.development.local file and the instance of the CMS running

Follow the example of .env.example file and fill the variables with the values provied by the dev.

Follow the instructions to run the CMS [here](https://gitlab.com/elaniin1/foodies/shop/cms/-/blob/develop/README.md)

## Now you can run the project

In the project directory, you can run:

### `npm run dev`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.\
You may also see any lint errors in the console.

### `npm run test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).
