import React, { useEffect, useState } from 'react';
import MenuHeroSection from '../components/MenuHeroSection/MenuHeroSection';
import MenuFilter from '../components/Menu/MenuFilter/MenuFilter';
import MenuList from '../components/Menu/MenuList';
import MenuPagination from '../components/Menu/MenuFilter/MenuPagination';
import Navbar from '../components/common/Navbar/Navbar';
import { getDishes } from '../queries/getDishes.query';
import useFetchAndLoad from '../hooks/useFetchAndLoad';
import Footer from '../components/common/Footer/Footer';
import DishNotFound from '../components/Menu/DishNotFound/DishNotFound';

function MenuPage() {
  const [dishes, setDishes] = useState([]);
  const [searchParams, setSearchParams] = useState({
    limit: 16,
    page: 1,
    search: '',
    categories: [],
  });

  const [totalPages, setTotalPages] = useState(0);

  const { callEndpoint } = useFetchAndLoad();

  const handleSearch = (params) => {
    setSearchParams((prev) => ({ ...prev, ...params }));
  };

  useEffect(() => {
    const fetchDishes = async () => {
      const response = await callEndpoint(await getDishes(searchParams));

      if (response.status === 200) {
        setDishes(response.data.data);
        setTotalPages(response.data.meta.last_page);
      }
    };
    fetchDishes();
  }, [searchParams]);

  return (
    <div className="bg-black ">
      <Navbar variant="black" className="relative z-30" />
      <MenuHeroSection />
      <section className="pt-[25px] px-4 bg-white">
        <MenuFilter onSearch={handleSearch} />
        {dishes.length > 0 ? (
          <>
            <MenuList dishes={dishes} />
            <MenuPagination
              totalPages={totalPages}
              currentPage={searchParams.page}
              setCurrentPage={setSearchParams}
            />
          </>
        ) : (
          <DishNotFound />
        )}
      </section>
      <Footer />
    </div>
  );
}

export default MenuPage;
