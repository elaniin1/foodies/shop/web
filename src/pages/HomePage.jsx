import React from 'react';
import Navbar from '../components/common/Navbar/Navbar';
import HeroSection from '../components/HeroSection/HeroSection';
import AboutSection from '../components/AboutSection/AboutSection';
import FinderSection from '../components/FinderSection/FinderSection';
import TestimonialsSection from '../components/TestimonialsSection/TestimonialsSection';
import ReviewSection from '../components/ReviewSection/ReviewSection';
import BenefitsAppSection from '../components/BenefitsAppSection/BenefitsAppSection';
import Footer from '../components/common/Footer/Footer';

function HomePage() {
  return (
    <>
      <Navbar variant="white" />
      <HeroSection id="hero" />
      <AboutSection id="about" />
      <FinderSection id="finder" />
      <TestimonialsSection id="testimonials" />
      <ReviewSection id="review" />
      <BenefitsAppSection id="benefits" />
      <Footer />
    </>
  );
}

export default HomePage;
