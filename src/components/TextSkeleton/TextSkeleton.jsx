import React from 'react';

function TextSkeleton() {
  return (
    <div className="w-full animate-pulse md:w-1/2">
      <div className="h-8 bg-gray-200 rounded-full max-w-xs mb-4"></div>
      <div className="h-8 bg-gray-200 rounded-full max-w-lg mb-4"></div>
      <div className="h-3 bg-gray-200 rounded-full max-w-[480px] mb-2.5"></div>
      <div className="h-3 bg-gray-200 rounded-full max-w-[420px] mb-2.5"></div>
      <div className="h-3 bg-gray-200 rounded-full max-w-[440px] mb-2.5"></div>
      <div className="h-3 bg-gray-200 rounded-full max-w-[460px] mb-2.5"></div>
      <div className="h-3 bg-gray-200 rounded-full max-w-[360px]"></div>
    </div>
  );
}

export default TextSkeleton;
