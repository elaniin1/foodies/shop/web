import React from 'react';
import PropTypes from 'prop-types';
import AboutBgSVG from '../../assets/img/aboutbg.svg';

function AboutBackground({ className }) {
  return (
    <object
      type="image/svg+xml"
      data={AboutBgSVG}
      width="100%"
      className={className}
    >
      background
    </object>
  );
}

AboutBackground.propTypes = {
  className: PropTypes.string,
};

AboutBackground.defaultProps = {
  className: '',
};

export default AboutBackground;
