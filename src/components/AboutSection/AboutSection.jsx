import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import AboutBackground from './AboutBackground';
import Title from '../common/Title/Title';
import Subtitle from '../common/Subtitle/Subtitle';
import Text from '../common/Text/Text';
import ArrowRight from '../Icons/ArrowRight';
import SectionLink from '../common/SectionLink/SectionLink';
import AboutSectionSkeleton from './AboutSectionSkeleton';
import useFetchAndLoad from '../../hooks/useFetchAndLoad';
import getCMSContent from '../../queries/getCMSContent';

const DEV_CSM_URL = import.meta.env.VITE_CMS_API;
function AboutSection({ id }) {
  const { callEndpoint, loading } = useFetchAndLoad();
  const [content, setContent] = useState({});

  useEffect(() => {
    const fetchLandingHeroSectionContent = async () => {
      const response = await callEndpoint(
        await getCMSContent('landing-about-section'),
      );
      if (response.status === 200) {
        setContent(response.data.data.attributes);
      }
    };
    fetchLandingHeroSectionContent();
  }, []);

  if (loading) {
    return (
      <section
        id={id}
        className="flex flex-col justify-between gap-10
        md:mt-[205px] md:flex-col md:items-center
        lg:mt-[283px] lg:flex-row lg:items-center"
      >
        <AboutSectionSkeleton />
      </section>
    );
  }

  return (
    <section
      id={id}
      className="flex flex-col justify-between mt-[52px] overflow-x-hidden
       md:mt-[205px] md:flex-col md:items-center
       lg:mt-[283px] lg:flex-row lg:items-center"
    >
      <div className="relative">
        <img
          src={
            content?.image?.data?.attributes.url.includes('http')
              ? content?.image?.data?.attributes.url
              : `${DEV_CSM_URL}${content?.image?.data?.attributes.url}`
          }
          alt="people enjoying food"
          className="h-auto min-w-[816px] -translate-x-[170px]
          sm:translate-x-0 sm:w-screen
          md:translate-x-0 md:w-screen md:min-w-0
          lg:translate-x-0 lg:max-w-[50vw]"
        />
        <Title
          className="absolute bottom-[30px] mr-4 ml-4 p-1 text-right text-white text-[35px]
          md:text-[40px] md:max-w-[579px] md:right-[53px] md:m-0
          lg:text-[50px]"
        >
          {content.title_normal_part}
          <span className="text-[#FFD600]">
            {' '}
            {content.title_highlighted_part}
          </span>
        </Title>
      </div>

      <div className="relative grow">
        <div className="mx-4 h-auto sm:h-[295px] md:h-[410px] md:mx-[53px] lg:max-w-[550px] lg:h-auto xl:max-h-fit xl:h-auto">
          <Subtitle className="text-[22px] leading-[26px] mt-[60px]">
            {content.about_subtitle}
          </Subtitle>
          <Text className="text-lg font-normal leading-6 mt-[27px]">
            {content.about_text}
          </Text>
          <SectionLink
            to={content.to_section_route || ''}
            label={content.to_section_label || ''}
            className="text-[22px] mt-[27px]"
            rightIcon={<ArrowRight />}
          />
        </div>
        <AboutBackground className="absolute -z-50 top-0 hidden sm:block lg:top-1 xl:top-2" />
      </div>
    </section>
  );
}

AboutSection.propTypes = {
  id: PropTypes.string,
};

AboutSection.defaultProps = {
  id: '',
};

export default AboutSection;
