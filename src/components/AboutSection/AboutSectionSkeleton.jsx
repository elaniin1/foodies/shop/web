import React from 'react';
import TextSkeleton from '../TextSkeleton/TextSkeleton';
import ImageSkeleton from '../ImageSkeleton/ImageSkeleton';

function AboutSectionSkeleton() {
  return (
    <>
      <ImageSkeleton />
      <TextSkeleton />
    </>
  );
}

export default AboutSectionSkeleton;
