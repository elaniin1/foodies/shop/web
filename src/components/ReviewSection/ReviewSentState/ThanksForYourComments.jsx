import React from 'react';
import { Link } from 'react-router-dom';
import ThanksBackground from '../../Backgrounds/ReviewSectionBackgrounds/ThanksBackground';

function ThanksForYourComments() {
  return (
    <div className="flex flex-col gap-8 items-center justify-center h-full py-72">
      <ThanksBackground />
      <h2 className="font-druk text-4xl text-white">
        Gracias por tus comentarios
      </h2>
      <p className="font-openSans text-2xl max-w-sm text-center text-white">
        Don&apos;t miss out on our great offers & Receive deals from all our top
        restaurants via e-mail.
      </p>

      <Link to="/menu" className="bg-[#FFD600] px-5 py-3 rounded font-bold">
        Conoce nuestro menu
      </Link>
    </div>
  );
}

export default ThanksForYourComments;
