import React from 'react';
import PropTypes from 'prop-types';
import TextField from '../common/TextField/TextField';
import TextArea from '../common/TextArea/TextArea';
import useFetchAndLoad from '../../hooks/useFetchAndLoad';
import { postReview } from '../../queries/postReview';

function ReviewForm({ className, onSend }) {
  const { callEndpoint } = useFetchAndLoad();

  const handleSubmit = async (e) => {
    e.preventDefault();

    const { name, email, message } = e.target.elements;

    if (!name.value || !email.value || !message.value) {
      return;
    }

    const contactInfo = {
      name: name.value,
      email: email.value,
      message: message.value,
    };

    const response = await callEndpoint(await postReview(contactInfo));

    if (response.status === 201) {
      onSend(true);
    }
  };
  return (
    <form className={className} onSubmit={handleSubmit}>
      <div className="grow lg:max-w-[300px]">
        <TextField
          id="name"
          type="text"
          label="Nombre y Apellido"
          placeholder="John Doe"
          inputStyle="focus:border-[#FFC700] outline-none lg:max-w-[300px]"
          labelStyle="text-xs w-fit"
          className="focus-within:text-[#FFC700] "
          required
          invalidMessage="Por favor ingrese su nombre y apellido"
        />
        <TextField
          id="email"
          type="email"
          label="Correo electrónico"
          placeholder="j.doe@correo.com"
          inputStyle="focus:border-[#FFC700] outline-none lg:max-w-[300px]"
          labelStyle="text-xs"
          className="focus-within:text-[#FFC700] mt-[10px]"
          required
          invalidMessage="Por favor ingrese un correo electrónico válido"
        />
      </div>
      <div className="grow lg:max-w-[500px]">
        <TextArea
          id="message"
          label="Mensaje"
          placeholder="El día de ahora mi experiencia fue..."
          inputStyle="focus:border-[#FFC700] outline-none xl:max-w-[500px] lg:h-[139px]"
          labelStyle="text-xs"
          className="focus-within:text-[#FFC700] mt-[10px] lg:mt-0"
          required
          invalidMessage="Por favor ingrese su mensaje"
        />
        <div
          className="flex justify-center lg:justify-end pb-[358px]
          md:pb-[378px]
          lg:pb-[112px]"
        >
          <button
            type="submit"
            className="font-openSans font-bold bg-[#FFD600] py-[14px] px-[22.5px] rounded mt-10 hover:bg-[#FFF1BF]
            sm:mt-[57px]
            lg:mt-[20px]"
          >
            Enviar comentarios
          </button>
        </div>
      </div>
    </form>
  );
}

ReviewForm.propTypes = {
  className: PropTypes.string,
  onSend: PropTypes.func.isRequired,
};

ReviewForm.defaultProps = {
  className: '',
};

export default ReviewForm;
