import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import ReviewForm from './ReviewForm';
import Title from '../common/Title/Title';
import Text from '../common/Text/Text';
import ThanksForYourComments from './ReviewSentState/ThanksForYourComments';
import ReviewSectionSkeleton from './ReviewSectionSkeleton';
import useFetchAndLoad from '../../hooks/useFetchAndLoad';
import getCMSContent from '../../queries/getCMSContent';

function ReviewSection({ id }) {
  const [isSent, setIsSent] = useState(false);
  const { callEndpoint, loading } = useFetchAndLoad();
  const [content, setContent] = useState({});

  useEffect(() => {
    const fetchLandingHeroSectionContent = async () => {
      const response = await callEndpoint(
        await getCMSContent('landing-review-section'),
      );
      if (response.status === 200) {
        setContent(response.data.data.attributes);
      }
    };
    fetchLandingHeroSectionContent();
  }, []);

  if (loading) {
    return (
      <section
        id={id}
        className="text-center bg-black pt-[73px] px-4
        sm:px-[67px]"
      >
        <ReviewSectionSkeleton />
      </section>
    );
  }

  return (
    <section
      id={id}
      className=" text-center bg-black pt-[73px] px-4
      sm:px-[67px]"
    >
      {!isSent ? (
        <>
          <Title className="text-[35px] text-white md:text-[36px]">
            {content.title}
          </Title>
          <Text
            color="#ffffff"
            className="max-w-[300px] text-lg mx-auto mt-5 text-white
        sm:text-[24px] sm:max-w-[520px]
        md:mt-[17px]
        lg:max-w-[700px]"
          >
            {content.text}
          </Text>
          <ReviewForm
            className="lg:flex justify-center items-top mt-[39px] sm:mt-5 lg:mt-10 gap-[50px]"
            onSend={setIsSent}
          />
        </>
      ) : (
        <ThanksForYourComments />
      )}
    </section>
  );
}

ReviewSection.propTypes = {
  id: PropTypes.string.isRequired,
};

export default ReviewSection;
