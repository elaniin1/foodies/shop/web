import React from 'react';

function ReviewSectionSkeleton() {
  return (
    <div className="w-full animate-pulse md:w-1/2 h-[340px] mx-auto">
      <div className="h-8 bg-gray-200 rounded-full max-w-xs mb-4 mx-auto"></div>
      <div className="h-8 bg-gray-200 rounded-full max-w-lg mb-4 mx-auto"></div>
      <div className="w-full h-40 bg-gray-200 rounded-full mb-4"></div>
    </div>
  );
}

export default ReviewSectionSkeleton;
