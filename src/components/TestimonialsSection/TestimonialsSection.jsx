import React from 'react';
import Carousel from '../common/Carousel/Carousel';
import TestimonialsBackgroundLeft from '../Backgrounds/TestimonialsBackgrounds/TestimonialsBackgroundLeft';
import TestimonialsBackgroundRight from '../Backgrounds/TestimonialsBackgrounds/TestimonialsBackgroundRight';

function TestimonialsSection({ id }) {
  return (
    <section
      id={id}
      className="flex min-h-[330px] justify-center relative overflow-x-clip bg-transparent
      sm:min-h-[640px] md:pt-2
      lg:min-h-[647px] lg:pt-20"
    >
      <TestimonialsBackgroundLeft className="absolute left-0" />
      <Carousel />
      <TestimonialsBackgroundRight className="absolute right-0 z-10" />
    </section>
  );
}

export default TestimonialsSection;
