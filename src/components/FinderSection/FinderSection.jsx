import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import Title from '../common/Title/Title';
import FinderControls from './FinderControls';
import LocationsList from './LocationsList';
import ErrorSearch from '../common/ErrorSearch/ErrorSearch';
import FinderSectionSkeleton from './FinderSectionSkeleton';
import useFetchAndLoad from '../../hooks/useFetchAndLoad';
import getCMSContent from '../../queries/getCMSContent';

const DEV_CSM_URL = import.meta.env.VITE_CMS_API;
function FinderSection({ id }) {
  const [locations, setLocations] = useState([]);
  const { callEndpoint, loading } = useFetchAndLoad();
  const [content, setContent] = useState({});
  useEffect(() => {
    const fetchLandingHeroSectionContent = async () => {
      const response = await callEndpoint(
        await getCMSContent('landing-finder-section'),
      );
      if (response.status === 200) {
        setContent(response.data.data.attributes);
      }
    };
    fetchLandingHeroSectionContent();
  }, []);

  if (loading) {
    return (
      <section
        id={id}
        className="flex flex-col justify-between items-center mt-[27px] gap-10 mb-20
        md:flex-col md:items-center
        lg:mt-56 lg:flex-row lg:items-center"
      >
        <FinderSectionSkeleton />
      </section>
    );
  }

  return (
    <section
      id={id}
      className="flex flex-col justify-between overflow-x-hidden mt-[27px]
      md:-mt-5 md:flex-col md:items-center
      lg:mt-20 lg:flex-row lg:items-start"
    >
      <div>
        <Title
          className="mr-4 ml-4 p-1 text-left text-[35px] max-w-[295px]
          sm:max-w-none sm:w-screen
          md:text-[40px] md:max-w-none md:w-screen md:right-[53px] md:m-0 md:pl-[53px]
          lg:text-[40px] lg:pl-[102px] lg:max-w-[676px] "
        >
          {content.title}
        </Title>
        <FinderControls onSearch={setLocations} />
        {locations.length > 0 ? (
          <LocationsList locations={locations} />
        ) : (
          <ErrorSearch />
        )}
      </div>
      <img
        src={
          content?.image?.data?.attributes.url.includes('http')
            ? content?.image?.data?.attributes.url
            : `${DEV_CSM_URL}${content?.image?.data?.attributes.url}`
        }
        alt="Mapa de ubicación"
        className="md:w-full xl:max-h-none lg:h-[609px] w-auto hidden sm:block md:mt-10 lg:mt-0 sm:mt-10"
      />
    </section>
  );
}

FinderSection.propTypes = {
  id: PropTypes.string,
};

FinderSection.defaultProps = {
  id: '',
};

export default FinderSection;
