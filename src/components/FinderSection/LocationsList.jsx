import React from 'react';
import PropTypes from 'prop-types';
import LocationItem from './LocationItem';

function LocationsList({ locations }) {
  return (
    <ul className="mt-[15px] mx-4">
      {locations.map((location) => (
        <LocationItem key={location.id} location={location} />
      ))}
    </ul>
  );
}

LocationsList.propTypes = {
  locations: PropTypes.arrayOf(Object).isRequired,
};

export default LocationsList;
