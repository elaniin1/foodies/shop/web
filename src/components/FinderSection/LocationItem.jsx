import React from 'react';
import PropTypes from 'prop-types';
import Subtitle from '../common/Subtitle/Subtitle';
import Text from '../common/Text/Text';

function LocationItem({ location }) {
  const openTimes = location.description.substring(0, 33);
  const address = location.description.substring(33);

  return (
    <li
      className="border border-black rounded-[4px] max-w-none max-h-max mt-[10px] pl-3 pt-[15px] mx-auto pb-[19px] hover:bg-[#FFF1BF] hover:border-none
      sm:max-w-[652px] sm:pb-[14px]
      md:max-w-[652px]
      lg:max-w-[552px] lg:max-h-[102px]"
    >
      <a href={location.url} target="_blank" rel="noopener noreferrer">
        <Subtitle>{location.title}</Subtitle>
        <Text className="text-black/100 text-sm mt-1">{openTimes}</Text>
        <Text className="text-black/100 text-sm mt-1">{address}</Text>
      </a>
    </li>
  );
}

LocationItem.propTypes = {
  location: PropTypes.shape({
    id: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    url: PropTypes.string,
    type: PropTypes.string.isRequired,
  }).isRequired,
};

export default LocationItem;
