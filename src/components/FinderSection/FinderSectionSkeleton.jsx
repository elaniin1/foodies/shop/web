import React from 'react';
import TextSkeleton from '../TextSkeleton/TextSkeleton';
import ImageSkeleton from '../ImageSkeleton/ImageSkeleton';

function FinderSectionSkeleton() {
  return (
    <>
      <TextSkeleton />
      <ImageSkeleton />
    </>
  );
}

export default FinderSectionSkeleton;
