import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import FoodIcon from '../Icons/FoodIcon';
import TruckIcon from '../Icons/TruckIcon';
import SearchIcon from '../Icons/SearchIcon';
import { getLocations } from '../../queries/getLocations.query';
import useFetchAndLoad from '../../hooks/useFetchAndLoad';

const OPTIONS = {
  takeout: 'takeout',
  delivery: 'delivery',
};

function FinderControls({ onSearch }) {
  const [type, setType] = useState(OPTIONS.takeout);
  const [input, setInput] = useState('');

  const { callEndpoint } = useFetchAndLoad();

  const handleSelect = (selection) => {
    setType(selection);
  };

  const handleSearch = async (locationType, term) => {
    const response = await callEndpoint(await getLocations(locationType, term));

    if (response.status === 200) {
      onSearch(response.data.data);
    }
  };

  useEffect(() => {
    handleSearch(type, input);
  }, [type]);

  return (
    <div className="mt-[30px] w-full lg:w-[676px]">
      <div className="flex">
        {/* TODO: Action button with icon */}
        <button
          type="button"
          onClick={() => handleSelect(OPTIONS.takeout)}
          className={`h-[50px] font-syne flex justify-center items-center gap-[10px] border border-[#C4C4C4] grow  ${
            type === OPTIONS.takeout
              ? 'text-white bg-black border-black '
              : 'hover:bg-gray-200 '
          }}`}
        >
          <FoodIcon color={type === OPTIONS.takeout ? 'white' : 'black'} />
          Para llevar
        </button>
        <button
          type="button"
          onClick={() => handleSelect(OPTIONS.delivery)}
          className={`h-[50px] font-syne flex justify-center items-center gap-[10px] border border-[#C4C4C4] grow ${
            type === OPTIONS.delivery
              ? 'text-white bg-black border-black '
              : 'hover:bg-gray-200 '
          }} `}
        >
          <TruckIcon
            color={`${type === OPTIONS.delivery ? 'white' : 'black'}`}
          />
          Domicilio
        </button>
      </div>

      <div className="flex items-center border-b-[1px] border-l-[1px] border-r-[1px] border-[#C4C4C4] pl-[15px]">
        <button type="button" onClick={() => handleSearch(type, input)}>
          <SearchIcon />
        </button>
        <input
          value={input}
          type="text"
          placeholder="Buscar nombre o dirección"
          className="h-[50px] w-full ml-[13px]"
          onChange={(e) => setInput(e.target.value)}
          onKeyDown={(e) => {
            if (e.key === 'Enter') {
              handleSearch(type, input);
            }
          }}
        />
      </div>
    </div>
  );
}

FinderControls.propTypes = {
  onSearch: PropTypes.func.isRequired,
};

export default FinderControls;
