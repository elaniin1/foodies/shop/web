import React from 'react';
import PropTypes from 'prop-types';
import Text from '../common/Text/Text';

function BenefitItem({ title, text, number, className }) {
  return (
    <div className={className}>
      <h2 className="text-2xl w-fit font-bold text-white px-3 py-2 bg-[#FFD600] rounded-full mx-auto">
        {number}
      </h2>
      <h2 className="text-2xl mt-7 font-bold font-openSans">{title}</h2>
      <Text
        color="black/1000"
        className="text-lg text-black/100 mt-10 max-w-xs mx-auto leading-5 font-roboto"
      >
        {text}
      </Text>
    </div>
  );
}

BenefitItem.propTypes = {
  title: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
  number: PropTypes.string.isRequired,
  className: PropTypes.string,
};

BenefitItem.defaultProps = {
  className: '',
};

export default BenefitItem;
