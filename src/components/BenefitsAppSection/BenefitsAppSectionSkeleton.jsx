import React from 'react';
import ImageSkeleton from '../ImageSkeleton/ImageSkeleton';

function BenefitsAppSectionSkeleton() {
  return (
    <>
      <ImageSkeleton />
      <div className="w-full grid grid-cols-1 md:grid-cols-2 gap-14 max-w-2xl mx-auto ">
        <div className="w-full animate-pulse md:w-1/2  mx-auto my-auto">
          <div className="h-8 bg-gray-200 rounded-full max-w-xs mb-4 mx-auto "></div>
          <div className="h-8 bg-gray-200 rounded-full max-w-lg mb-4 mx-auto"></div>
        </div>
        <div className="w-full animate-pulse md:w-1/2  mx-auto my-auto">
          <div className="h-8 bg-gray-200 rounded-full max-w-xs mb-4 mx-auto"></div>
          <div className="h-8 bg-gray-200 rounded-full max-w-lg mb-4 mx-auto"></div>
        </div>
        <div className="w-full animate-pulse md:w-1/2 mx-auto md:col-span-2 my-auto">
          <div className="h-8 bg-gray-200 rounded-full max-w-xs mb-4 mx-auto"></div>
          <div className="h-8 bg-gray-200 rounded-full max-w-lg mb-4 mx-auto"></div>
        </div>
      </div>
    </>
  );
}

export default BenefitsAppSectionSkeleton;
