import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import Title from '../common/Title/Title';
import BenefitItem from './BenefitItem';
import BenefitsArrowBackgroundLeft from '../Backgrounds/BenefitsSectionBackgrounds/BenefitsArrowBackgroundLeft';
import BenefitsArrowBackgroundRight from '../Backgrounds/BenefitsSectionBackgrounds/BenefitsArrowBackgroundRight';
import BenefitsAppSectionSkeleton from './BenefitsAppSectionSkeleton';
import useFetchAndLoad from '../../hooks/useFetchAndLoad';
import getCMSContent from '../../queries/getCMSContent';

const DEV_CSM_URL = import.meta.env.VITE_CMS_API;
function Benefit({ id }) {
  const { callEndpoint, loading } = useFetchAndLoad();
  const [content, setContent] = useState({});

  useEffect(() => {
    const fetchLandingHeroSectionContent = async () => {
      const response = await callEndpoint(
        await getCMSContent('landing-benefits-app-section'),
      );
      if (response.status === 200) {
        setContent(response.data.data.attributes);
      }
    };
    fetchLandingHeroSectionContent();
  }, []);

  if (loading) {
    return (
      <section
        id={id}
        className="text-center pt-[73px] px-4 flex flex-col justify-center overflow-y-visible gap-10
        sm:px-[67px]
        xl:flex-row"
      >
        <BenefitsAppSectionSkeleton />
      </section>
    );
  }

  return (
    <section
      id={id}
      className="text-center pt-[73px] px-4 flex flex-col justify-center overflow-y-visible
      sm:px-[67px]
      xl:flex-row"
    >
      <div className="flex shrink overflow-x-clip">
        <img
          src={
            content?.image?.data?.attributes.url.includes('http')
              ? content?.image?.data?.attributes.url
              : `${DEV_CSM_URL}${content?.image?.data?.attributes.url}`
          }
          alt="A phone with Foodies app open"
          className="-mt-80 ml-10 sm:ml-20 xl:ml-0 md:-mt-44"
        />
      </div>

      <div className="z-50 shrink-0 xl:-ml-60">
        <Title className="text-3xl md:text-4xl max-w-xl mx-auto relative">
          {content.title}
          <div
            className="absolute h-20 w-full bg-[#FFD600] -bottom-2 -z-50 -rotate-1
          md:h-12 md:-bottom-1"
          />
        </Title>

        <div className="relative grid grid-cols-1 md:grid-cols-2 gap-14 max-w-2xl mx-auto mt-16">
          {content?.app_benefits?.data?.map((benefit) => (
            <BenefitItem
              key={benefit.id}
              title={benefit.attributes.title}
              text={benefit.attributes.text}
              number={benefit.attributes.number}
              className={
                benefit.attributes.number === '03' ? 'md:col-span-2' : ''
              }
            />
          ))}
          <BenefitsArrowBackgroundLeft />
          <BenefitsArrowBackgroundRight />
        </div>
      </div>
    </section>
  );
}

Benefit.propTypes = {
  id: PropTypes.string.isRequired,
};

export default Benefit;
