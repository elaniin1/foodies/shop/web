import React from 'react';
import MenuDesktopBackground from '../Backgrounds/MenuHeroBackground/MenuDesktopBackground';
import MenuTabletBackground from '../Backgrounds/MenuHeroBackground/MenuTabletBackground';
import MenuMobileBackground from '../Backgrounds/MenuHeroBackground/MenuMobileBackground';

function HeroBackground() {
  return (
    <>
      <MenuDesktopBackground className="absolute -top-5 right-0 -z-50 hidden lg:block" />
      <MenuTabletBackground className="absolute -top-2 right-0 -z-50 hidden md:block lg:hidden" />
      <MenuMobileBackground className="absolute -top-10 right-0 -z-50 md:hidden" />
    </>
  );
}

export default HeroBackground;
