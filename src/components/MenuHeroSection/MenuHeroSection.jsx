import React, { useState, useEffect } from 'react';
import Title from '../common/Title/Title';
import MenuHeroBackground from './MenuHeroBackground';
import MenuHeroSectionSkeleton from './MenuHeroSectionSkeleton';
import useFetchAndLoad from '../../hooks/useFetchAndLoad';
import getCMSContent from '../../queries/getCMSContent';

const DEV_CSM_URL = import.meta.env.VITE_CMS_API;

function MenuHeroSection() {
  const { callEndpoint, loading } = useFetchAndLoad();
  const [content, setContent] = useState({});

  useEffect(() => {
    const fetchLandingHeroSectionContent = async () => {
      const response = await callEndpoint(
        await getCMSContent('menu-hero-section'),
      );
      if (response.status === 200) {
        setContent(response.data.data.attributes);
      }
    };
    fetchLandingHeroSectionContent();
  }, []);

  if (loading) {
    return (
      <section
        className="flex flex-col-reverse pl-5 pb-[106px] justify-between overflow-hidden
        md:pl-[53px] md:flex-row md:items-center md:pb-[110px]
        lg:pl-[102px] ld:flex-row lg:items-center lg:pb-0 text-white bg-black"
      >
        <MenuHeroSectionSkeleton />
      </section>
    );
  }

  return (
    <section
      className="flex flex-col-reverse pl-5 pb-[106px] justify-between overflow-hidden
    md:pl-[53px] md:flex-row md:items-center md:pb-[110px]
    lg:pl-[102px] ld:flex-row lg:items-center lg:pb-0 text-white bg-black"
    >
      <div className="mt-[64px] self-center">
        <div className="relative max-w-[343px] md:max-w-[360px] lg:max-w-[600px] lg:w-[560px] z-10">
          <Title className="text-[35px] md:text-[40px] lg:text-6xl">
            {content.title}
            <span className="text-black"> {content.title_black_part}</span>
          </Title>
          <div
            className="bg-[#FFD600] h-24 w-[295px] xs:h-[54px] absolute -rotate-[0.64deg] -bottom-[11px] -translate-x-[6px] -z-50
            md:w-[337px] md:-translate-x-[6px] md:-bottom-[11px]
            lg:w-[510px] lg:-bottom-[6px]"
          />
        </div>
      </div>
      <div className="z-10">
        <img
          src={
            content?.image?.data?.attributes.url.includes('http')
              ? content?.image?.data?.attributes.url
              : `${DEV_CSM_URL}${content?.image?.data?.attributes.url}`
          }
          alt="regular hamburger"
          className="max-w-screen h-auto top-[121px] mt-[47px]
          md:mr-[30px] md:max-w-[340px] md:h-[268px]
          lg:h-[446px] lg:max-w-none xl:mr-24"
        />
        <MenuHeroBackground />
      </div>
    </section>
  );
}

export default MenuHeroSection;
