import React from 'react';
import MenuHeroBackground from './MenuHeroBackground';
import TextSkeleton from '../TextSkeleton/TextSkeleton';
import ImageSkeleton from '../ImageSkeleton/ImageSkeleton';

function MenuHeroSectionSkeleton() {
  return (
    <>
      <TextSkeleton />
      <ImageSkeleton />
      <MenuHeroBackground />
    </>
  );
}

export default MenuHeroSectionSkeleton;
