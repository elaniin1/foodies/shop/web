import React from 'react';
import ProTypes from 'prop-types';
import KetchupImage from '../../../assets/img/testimonials_bg_ketchup.png';

function TestimonialsBackgroundRight({ className }) {
  return (
    <div className={`hidden sm:block ${className}`}>
      <img
        src={KetchupImage}
        alt="a bottle of ketchup"
        className="hidden -mt-[23px] w-auto h-[762px] lg:block"
      />
      <div className="absolute top-[165px] -right-2 -z-50 lg:top-[90px]">
        <svg
          width="223"
          height="546"
          viewBox="0 0 214 547"
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path
            fillRule="evenodd"
            clipRule="evenodd"
            d="M180.734 8.41235C124.506 42.2103 39.6989 113.591 126.332 169.742C220.493 230.772 144.853 307.837 94.1885 329.726C42.4383 352.084 12.9208 376.961 4.99732 397.684C-18.3685 463.075 38.3835 600.109 213.763 524.926V320H223.763V284.529L223.763 229.997L223.763 150.678V0.438904L180.734 8.41235Z"
            fill="#FF2C2C"
          />
        </svg>
      </div>
    </div>
  );
}

TestimonialsBackgroundRight.propTypes = {
  className: ProTypes.string,
};

TestimonialsBackgroundRight.defaultProps = {
  className: '',
};

export default TestimonialsBackgroundRight;
