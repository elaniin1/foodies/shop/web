import React from 'react';

function TestimonialSkeleton() {
  return (
    <div className="w-full animate-pulse md:w-1/2">
      <div className="h-8 bg-gray-200 rounded-full max-w-xs mb-4 mx-auto"></div>
      <div className="h-8 bg-gray-200 rounded-full max-w-lg mb-4 mx-auto"></div>
    </div>
  );
}

export default TestimonialSkeleton;
