import React from 'react';
import PropTypes from 'prop-types';
import MenuItem from './MenuItem';

function MenuList({ dishes }) {
  return (
    <section
      className="mx-auto grid grid-cols-1 gap-x-5 gap-y-[30px] pt-10 px-4
      md:grid-cols-2 md:pt-[90px]
      xl:grid-cols-3 lg:pt-[111px]
      2xl:grid-cols-4"
    >
      {dishes.map((dish) => (
        <MenuItem key={dish.id} dish={dish} />
      ))}
    </section>
  );
}

MenuList.propTypes = {
  dishes: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string.isRequired,
      title: PropTypes.string.isRequired,
      description: PropTypes.string.isRequired,
      price: PropTypes.number.isRequired,
      image: PropTypes.string.isRequired,
      categories: PropTypes.arrayOf(
        PropTypes.shape({
          id: PropTypes.string.isRequired,
          name: PropTypes.string.isRequired,
          slug: PropTypes.string.isRequired,
        }),
      ),
    }),
  ).isRequired,
};

export default MenuList;
