import React from 'react';
import DishNotFoundBackground from '../../Backgrounds/DishNotFoundBackground';

function DishNotFound() {
  return (
    <div className="flex flex-col gap-2 items-center justify-center h-full pt-20">
      <DishNotFoundBackground />
      <h2 className="font-syne text-xl">¡Platillo no encontrado!</h2>
      <p className="font-openSans text-lg max-w-sm text-center">
        Te invitamos a que verifiques si el nombre esta bien escrito o prueba
        buscando un nuevo platillo.
      </p>
    </div>
  );
}

export default DishNotFound;
