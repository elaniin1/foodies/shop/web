import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import MenuSearchBar from './MenuSearchBar';
import MenuCategories from './MenuCategories';

function MenuFilter({ onSearch }) {
  const [searchParams, setSearchParams] = useState({
    q: '',
    categories: [],
  });

  const handleSearch = (params) => {
    if (params.term) {
      setSearchParams((prev) => ({ ...prev, q: params.term }));
    }

    if (params.categories) {
      setSearchParams((prev) => ({
        ...prev,
        categories: params.categories,
      }));
    }

    onSearch(searchParams);
  };

  const handleSetTerm = (q) => {
    setSearchParams((prev) => ({ ...prev, q }));
  };

  const handleSetCategories = (categories) => {
    setSearchParams((prev) => ({ ...prev, categories: [categories] }));
  };

  useEffect(() => {}, [searchParams]);

  return (
    <div
      className="flex justify-start gap-5 items-center
      md:flex-col
      lg:gap-10 lg:flex-row"
    >
      <MenuSearchBar
        onSearch={handleSearch}
        term={searchParams.q}
        setTerm={handleSetTerm}
      />
      <MenuCategories
        onSearch={handleSearch}
        selectedCategories={searchParams.categories}
        setSelectedCategories={handleSetCategories}
      />
    </div>
  );
}

MenuFilter.propTypes = {
  onSearch: PropTypes.func.isRequired,
};

export default MenuFilter;
