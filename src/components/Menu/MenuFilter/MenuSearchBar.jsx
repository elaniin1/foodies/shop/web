import React from 'react';
import PropTypes from 'prop-types';
import SearchIcon from '../../Icons/SearchIcon';

function MenuSearchBar({ onSearch, term, setTerm }) {
  const handleSearch = async () => {
    onSearch({ term });
  };

  return (
    <div
      className="flex items-center border border-[#C4C4C4] rounded-lg pl-[15px] w-full
      md:max-w-[467px]"
    >
      <button type="button" onClick={handleSearch}>
        <SearchIcon />
      </button>
      <input
        value={term}
        type="text"
        placeholder="Buscar tu platillo favorito..."
        className="h-[50px] w-full ml-[13px]"
        onChange={(e) => setTerm(e.target.value)}
        onKeyDown={(e) => {
          if (e.key === 'Enter') {
            handleSearch();
          }
        }}
      />
    </div>
  );
}

MenuSearchBar.propTypes = {
  onSearch: PropTypes.func.isRequired,
  term: PropTypes.string.isRequired,
  setTerm: PropTypes.func.isRequired,
};

export default MenuSearchBar;
