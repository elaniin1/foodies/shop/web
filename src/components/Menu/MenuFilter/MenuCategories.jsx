import React, { useEffect, useState, useRef } from 'react';
import PropTypes from 'prop-types';
import Sliders from '../../Icons/Sliders';
import X from '../../Icons/X';
import { getMenuCategories } from '../../../queries/getMenuCategories.query';
import useFetchAndLoad from '../../../hooks/useFetchAndLoad';

function MenuCategories({ onSearch, setSelectedCategories }) {
  const [categories, setCategories] = useState([
    {
      id: '',
      name: 'Todas',
      slug: 'todas',
    },
  ]);
  const [selectedCategory, setSelectedCategory] = useState('');
  const [mobileFilters, setToggleMobileFilters] = useState('hidden');
  const [mobileFiltersZIndez, setMobileFiltersZIndex] = useState('z-20');
  const body = useRef(document.querySelector('body'));

  const { callEndpoint } = useFetchAndLoad();

  const toggleScroll = () => {
    if (body.current.style.overflow === 'hidden') {
      body.current.style.overflow = 'auto';
    } else {
      body.current.style.overflow = 'hidden';
    }
  };

  const toggleMenu = () => {
    // Toggle mobile filters
    setToggleMobileFilters((prev) => {
      if (prev === 'hidden') return '';
      return 'hidden';
    });

    // Toggle mobile filters z-index
    setMobileFiltersZIndex((prev) => {
      if (prev === 'z-20') return 'z-50';
      return 'z-20';
    });

    // Toggle all app scroll
    toggleScroll();
  };

  const handleMobileFiltersOpen = () => {
    toggleMenu();
  };

  const handleCategoryClick = (event) => {
    const category = event.target.value;
    setSelectedCategory(category);
  };

  const resetMobileFilterState = () => {
    setToggleMobileFilters('hidden');
    body.current.style.overflow = 'auto';
  };

  const resetMobileFilters = () => {
    resetMobileFilterState();
  };

  window.addEventListener('resize', resetMobileFilters);

  useEffect(() => {
    const fetchCategories = async () => {
      const response = await callEndpoint(await getMenuCategories());
      if (response.status === 200) {
        setCategories([...categories, ...response.data.data]);
      }
    };
    fetchCategories();
  }, []);

  useEffect(() => {
    onSearch({ categories: [selectedCategory] });
  }, [selectedCategory]);

  return (
    <div className={`relative ${mobileFiltersZIndez}`}>
      <ul className="gap-10 hidden md:flex justify-center items-center">
        {categories.map((category) => (
          <li className="relative" key={category.id}>
            <button
              className="font-syne text-lg"
              type="button"
              value={category.id}
              onClick={(e) => {
                setSelectedCategories(e.target.value);
                handleCategoryClick(e);
              }}
            >
              {category.name}
            </button>
            {selectedCategory === category.id ? (
              <div className="absolute h-[6px] w-[110%] bottom-[40%] -z-10 -left-1 bg-[#FFD600]" />
            ) : null}
          </li>
        ))}
      </ul>

      <button
        className="h-[26px] w-[26px] cursor-pointer md:hidden mr-4 md:mr-10 self-center "
        type="button"
        onClick={handleMobileFiltersOpen}
      >
        <Sliders />
      </button>
      <section
        className={`fixed top-0 left-0 pt-[47px] pl-[17px] pr-4 min-h-screen justify-content-center z-50 w-full origin-top animate-open-menu flex-col bg-black text-white ${mobileFilters}`}
      >
        <header className="flex items-center justify-between" role="banner">
          <h1 className="font-druk font-bold text-[25px]  text-[#FFD600]">
            Filtros
          </h1>
          <button type="button" onClick={toggleMenu}>
            <X />
          </button>
        </header>
        <ul className="text-left flex flex-col items-left mt-[53px] gap-[50px]">
          {categories.map((category) => (
            <li className="relative w-fit" key={category.id}>
              <button
                className="font-syne text-[25px]"
                type="button"
                value={category.id}
                onClick={(e) => {
                  setSelectedCategories(e.target.value);
                  handleCategoryClick(e);
                  resetMobileFilters();
                }}
              >
                {category.name}
              </button>

              {selectedCategory === category.id ? (
                <div className="absolute h-[6px] w-[105%] bottom-2 bg-[#FFD600]" />
              ) : null}
            </li>
          ))}
        </ul>
      </section>
    </div>
  );
}

MenuCategories.propTypes = {
  onSearch: PropTypes.func.isRequired,
  setSelectedCategories: PropTypes.func.isRequired,
};

export default MenuCategories;
