import React from 'react';
import PropTypes from 'prop-types';

function MenuPagination({ totalPages, currentPage, setCurrentPage }) {
  return (
    <div className="flex flex-col w-full justify-center items-center gap-[10px] py-10 md:flex-row">
      {/* Pages buttons with number */}
      <div className="flex gap-[10px]">
        {[...Array(totalPages)].map((_, index) => (
          <button
            type="button"
            className={`font-openSans font-bold text-[20px] px-[18.5px] py-[9px] rounded-lg border ${
              currentPage === index + 1
                ? 'bg-black text-[#FFD600] border-none'
                : ''
            }`}
            // eslint-disable-next-line react/no-array-index-key
            key={index}
            onClick={
              () =>
                // eslint-disable-next-line implicit-arrow-linebreak
                setCurrentPage((prev) => ({ ...prev, page: index + 1 }))
              // eslint-disable-next-line react/jsx-curly-newline
            }
            disabled={currentPage === index + 1}
          >
            {index + 1}
          </button>
        ))}
      </div>

      <button
        type="button"
        className={`font-openSans font-bold text-[20px] py-[10px] px-[30px] bg-[#FFF1BF] rounded-lg ${
          currentPage === totalPages ? 'hidden' : ''
        }`}
        onClick={
          () =>
            // eslint-disable-next-line implicit-arrow-linebreak
            setCurrentPage((prev) => ({ ...prev, page: currentPage + 1 }))
          // eslint-disable-next-line react/jsx-curly-newline
        }
      >
        Siguiente
      </button>
    </div>
  );
}

MenuPagination.propTypes = {
  totalPages: PropTypes.number.isRequired,
  currentPage: PropTypes.number.isRequired,
  setCurrentPage: PropTypes.func.isRequired,
};

export default MenuPagination;
