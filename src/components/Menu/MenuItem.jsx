import React from 'react';
import PropTypes from 'prop-types';
import Subtitle from '../common/Subtitle/Subtitle';

function MenuItem({ dish }) {
  return (
    <div className="group max-w-[330px] hover:scale-110 transform transition duration-200 hover:cursor-pointer xs:mx-auto break-words">
      <div
        className="rounded-[10px]
      group-hover:shadow-2xl"
      >
        <div
          className="overflow-hidden rounded-[10px] flex
          group-hover:rounded-b-none grow shrink"
        >
          <img
            className="object-cover aspect-square max-h-[264px] w-full  h-full"
            src={dish.image}
            alt={dish.title}
          />
        </div>
        <section className="px-[10px] flex flex-col justify-between grow ">
          <section>
            <Subtitle className="p-[10px] text-[22px]">{dish.title}</Subtitle>
            <p className="p-[10px] font-openSans text-lg">{dish.description}</p>
          </section>

          <section className="flex flex-wrap justify-between items-center pb-5 px-5">
            {dish.categories.map((category) => (
              <p
                key={category.id}
                className="text-lg font-openSans text-[#78909C]"
              >
                {category.name}
              </p>
            ))}
            <div className="ml-auto">
              <p className="justify-self-end text-right bg-[#FFD600] py-[6px] px-[10px] font-bold font-druk rounded-lg">
                {(dish.price / 100).toLocaleString('en-US', {
                  style: 'currency',
                  currency: 'USD',
                })}
              </p>
            </div>
          </section>
        </section>
      </div>
    </div>
  );
}

MenuItem.propTypes = {
  dish: PropTypes.shape({
    id: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired,
    image: PropTypes.string.isRequired,
    categories: PropTypes.arrayOf(
      PropTypes.shape({
        id: PropTypes.string.isRequired,
        name: PropTypes.string.isRequired,
        slug: PropTypes.string.isRequired,
      }),
    ),
  }).isRequired,
};

export default MenuItem;
