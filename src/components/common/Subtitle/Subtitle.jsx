import React from 'react';
import PropTypes from 'prop-types';

function Subtitle({ className, children }) {
  return (
    <h3 data-testid="Subtitle" className={`font-syne font-bold ${className}`}>
      {children}
    </h3>
  );
}

Subtitle.propTypes = {
  className: PropTypes.string,
  children: PropTypes.node,
};

Subtitle.defaultProps = {
  className: '',
  children: null,
};

export default Subtitle;
