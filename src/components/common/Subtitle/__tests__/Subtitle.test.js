import React from 'react';
import { render, screen } from '@testing-library/react';
import Subtitle from '../Subtitle';

const MOCK_SUBTITLE = 'This is a subtitle';

describe('Subtitle', () => {
  it('renders correctly', () => {
    render(<Subtitle>{MOCK_SUBTITLE}</Subtitle>);

    expect(screen.getByTestId('Subtitle')).toBeInTheDocument();
  });

  it('renders with Subtitle', () => {
    render(<Subtitle>{MOCK_SUBTITLE}</Subtitle>);

    expect(screen.getByTestId('Subtitle')).toHaveTextContent(MOCK_SUBTITLE);
  });

  it('renders with className', () => {
    const MOCK_CLASS_NAME = 'mock-class-name';
    render(<Subtitle className={MOCK_CLASS_NAME}>{MOCK_SUBTITLE}</Subtitle>);

    expect(screen.getByTestId('Subtitle')).toHaveClass(MOCK_CLASS_NAME);
  });
});
