import React, { useState, useEffect } from 'react';
// eslint-disable-next-line import/no-unresolved
import 'swiper/css';
// eslint-disable-next-line import/no-unresolved
import 'swiper/css/navigation';
// eslint-disable-next-line import/no-unresolved
import 'swiper/css/pagination';
// eslint-disable-next-line import/no-unresolved
import { Swiper, SwiperSlide } from 'swiper/react';
import { Navigation, Pagination } from 'swiper';
import TestimonialSlide from './TestimonialSlide';
import TestimonialSkeleton from '../../TestimonialSkeleton/TestimonialSkeleton';
import useFetchAndLoad from '../../../hooks/useFetchAndLoad';
import getCMSContent from '../../../queries/getCMSContent';

function Carousel() {
  const { callEndpoint, loading } = useFetchAndLoad();
  const [content, setContent] = useState([]);

  useEffect(() => {
    const fetchLandingHeroSectionContent = async () => {
      const response = await callEndpoint(await getCMSContent('testimonials'));
      if (response.status === 200) {
        setContent(response.data.data);
      }
    };
    fetchLandingHeroSectionContent();
  }, []);

  if (loading) {
    return (
      <div className="w-full flex items-center justify-center">
        <TestimonialSkeleton />
      </div>
    );
  }

  return (
    <Swiper
      pagination={{
        type: 'fraction',
      }}
      navigation
      modules={[Pagination, Navigation]}
      className="mySwiper bg-transparent"
    >
      {content.map((testimonial) => (
        <SwiperSlide key={testimonial.id}>
          <TestimonialSlide
            title={testimonial.attributes.title}
            text={testimonial.attributes.text}
          />
        </SwiperSlide>
      ))}
    </Swiper>
  );
}

export default Carousel;
