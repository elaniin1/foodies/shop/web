import React from 'react';
import PropTypes from 'prop-types';
import Title from '../Title/Title';
import Text from '../Text/Text';

function TestimonialSlide({ title, text }) {
  return (
    <>
      <Title className="text-xl md:text-3xl ">“{title}”</Title>
      <Text className="md:text-lg">{text}</Text>
    </>
  );
}

TestimonialSlide.propTypes = {
  title: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
};

TestimonialSlide.defaultProps = {
  title: '',
  text: '',
};

export default TestimonialSlide;
