import React from 'react';
import PropTypes from 'prop-types';

function Text({ className, children, color }) {
  return (
    <p
      data-testid="text"
      className={`text-animated text text--folding font-openSans text-[${color}] ${className} `}
      data-scroll="out"
      data-splitting=""
    >
      {children}
    </p>
  );
}

Text.propTypes = {
  className: PropTypes.string,
  children: PropTypes.node,
  color: PropTypes.string,
};

Text.defaultProps = {
  className: '',
  children: null,
  color: '#00000066',
};

export default Text;
