import React from 'react';
import { render, screen } from '@testing-library/react';
import Text from '../Text';

const MOCK_TEXT = 'Hello World';

describe('Text', () => {
  it('Text renders correctly', () => {
    render(<Text>{MOCK_TEXT}</Text>);

    expect(screen.getByTestId('text')).toBeInTheDocument();
  });

  it('Text renders with text', () => {
    render(<Text>{MOCK_TEXT}</Text>);
    const text = screen.getByTestId('text').childNodes[0].nodeValue;

    expect(text).toBe(MOCK_TEXT);
  });

  it('Text renders with className', () => {
    const MOCK_CLASS_NAME = 'mock-class-name';
    render(<Text className={MOCK_CLASS_NAME} />);

    expect(screen.getByTestId('text')).toHaveClass(MOCK_CLASS_NAME);
  });

  it('it renders with color', () => {
    const MOCK_COLOR = '#ff0000';
    render(<Text color={MOCK_COLOR}>{MOCK_TEXT}</Text>);

    const className = screen.getByTestId('text').className;

    expect(className).toContain(`text-[${MOCK_COLOR}]`);
  });
});
