import React from 'react';
import PropTypes from 'prop-types';

function SectionLink({ to, label, leftIcon, rightIcon, className }) {
  return (
    <div className={`font-syne flex items-center gap-[5px] ${className}`}>
      {leftIcon}
      <a href={to}>{label}</a>
      {rightIcon}
    </div>
  );
}

SectionLink.propTypes = {
  to: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  leftIcon: PropTypes.node,
  rightIcon: PropTypes.node,
  className: PropTypes.string,
};

SectionLink.defaultProps = {
  className: '',
  leftIcon: null,
  rightIcon: null,
};

export default SectionLink;
