import React, { useEffect } from 'react';
import './animation.css';
import PropTypes from 'prop-types';
import ScrollOut from 'scroll-out';
import Splitting from 'splitting';

function Title({ className, children }) {
  useEffect(() => {
    ScrollOut({
      targets: ['.title-animated', '.text-animated'],
      once: true,
      threshold: 0.9,
    });

    Splitting();
  }, []);

  return (
    <h2
      data-testid="Title"
      className={`title-animated text text--folding font-druk leading-[35px] md:leading-10 lg:leading-[60px] ${className}`}
      data-scroll="out"
      data-splitting=""
    >
      {children}
    </h2>
  );
}

Title.propTypes = {
  className: PropTypes.string,
  children: PropTypes.node,
};

Title.defaultProps = {
  className: '',
  children: null,
};

export default Title;
