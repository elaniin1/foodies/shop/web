import React from 'react';
import { render, screen } from '@testing-library/react';
import Title from '../Title';

const MOCK_TITLE = 'This is a title';

describe('Title', () => {
  it('renders correctly', () => {
    render(<Title>{MOCK_TITLE}</Title>);

    expect(screen.getByTestId('Title')).toBeInTheDocument();
  });

  it('renders with Title', () => {
    render(<Title>{MOCK_TITLE}</Title>);

    expect(screen.getByTestId('Title')).toHaveTextContent(MOCK_TITLE);
  });

  it('renders with className', () => {
    const MOCK_CLASS_NAME = 'mock-class-name';
    render(<Title className={MOCK_CLASS_NAME}>{MOCK_TITLE}</Title>);

    expect(screen.getByTestId('Title')).toHaveClass(MOCK_CLASS_NAME);
  });
});
