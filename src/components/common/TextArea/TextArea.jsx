import React, { useState } from 'react';
import PropTypes from 'prop-types';

function TextField({
  label,
  id,
  placeholder,
  labelStyle,
  inputStyle,
  required,
  invalidMessage,
  className,
}) {
  const [showInvalidMessage, setShowInvalidMessage] = useState('');
  return (
    <div className={`flex-col text-white/100 ${className}`}>
      <label className={`items-start justify-start ${labelStyle}`} htmlFor={id}>
        <p className="text-left font-openSans">{label}</p>
      </label>
      <textarea
        className={`block w-full bg-transparent border border-[#ffffff] rounded px-5 py-[14px] mt-[5px] ${inputStyle} `}
        id={id}
        name={id}
        placeholder={placeholder}
        required={required}
        onInvalid={(e) => {
          e.preventDefault();
          setShowInvalidMessage(true);
        }}
        onChange={() => {
          setShowInvalidMessage(false);
        }}
      />

      {required && showInvalidMessage ? (
        <p className="text-red-700 font-bold font-openSans text-sm">
          {invalidMessage}
        </p>
      ) : null}
    </div>
  );
}

TextField.propTypes = {
  label: PropTypes.string,
  id: PropTypes.string.isRequired,
  placeholder: PropTypes.string,
  labelStyle: PropTypes.string,
  inputStyle: PropTypes.string,
  required: PropTypes.bool,
  invalidMessage: PropTypes.string,
  className: PropTypes.string,
};

TextField.defaultProps = {
  label: '',
  placeholder: '',
  labelStyle: '',
  inputStyle: '',
  required: false,
  invalidMessage: '',
  className: '',
};

export default TextField;
