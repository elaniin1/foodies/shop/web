import React from 'react';
import PropTypes from 'prop-types';

function Hamburguer({ onHamburgerClick, closeButton }) {
  return (
    <button
      id="hamburger"
      className={`h-[26px] w-[26px] cursor-pointer lg:hidden mr-4 md:mr-10 self-center ${closeButton}`}
      type="button"
      onClick={onHamburgerClick}
    >
      <div
        className=" h-0.5 w-5 rounded bg-black
        transition-all duration-500 before:absolute before:h-0.5 before:w-5
        before:-translate-x-[10px] before:-translate-y-[6px] before:rounded before:bg-black
        before:transition-all before:duration-500 before:content-[''] after:absolute
        after:h-0.5 after:w-5 after:-translate-x-[10px] after:translate-y-[6px] after:rounded
        after:bg-black after:transition-all after:duration-500 after:content-['']"
      />
    </button>
  );
}

Hamburguer.propTypes = {
  onHamburgerClick: PropTypes.func.isRequired,
  closeButton: PropTypes.string.isRequired,
};

export default Hamburguer;
