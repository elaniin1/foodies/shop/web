import React from 'react';

function NavbarSkeleton() {
  return (
    <div className="pt-5 pl-10">
      <div className="w-full flex items-center justify-start">
        <div className="w-40 animate-pulse mx-auto my-auto">
          <div className="h-8 bg-gray-200 rounded-full max-w-xs mb-4 mx-auto "></div>
        </div>
        <div className="w-full flex items-center justify-center gap-10">
          <div className="w-40 animate-pulse mx-auto my-auto">
            <div className="h-3 bg-gray-200 rounded-full max-w-xs mb-4 mx-auto my-auto"></div>
          </div>
          <div className="w-40 animate-pulse mx-auto my-auto">
            <div className="h-3 bg-gray-200 rounded-full max-w-xs mb-4 mx-auto "></div>
          </div>
          <div className="w-40 animate-pulse mx-auto my-auto">
            <div className="h-3 bg-gray-200 rounded-full max-w-xs mb-4 mx-auto "></div>
          </div>
          <div className="w-40 animate-pulse mx-auto my-auto">
            <div className="h-3 bg-gray-200 rounded-full max-w-xs mb-4 mx-auto "></div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default NavbarSkeleton;
