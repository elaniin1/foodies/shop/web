import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

function NavItem({ to, label, className, onClick }) {
  return (
    <li className={`font-syne ${className}`}>
      <Link to={to} onClick={onClick}>
        {label}
      </Link>
    </li>
  );
}

NavItem.propTypes = {
  to: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  className: PropTypes.string,
  onClick: PropTypes.func,
};

NavItem.defaultProps = {
  className: '',
  onClick: () => {},
};

export default NavItem;
