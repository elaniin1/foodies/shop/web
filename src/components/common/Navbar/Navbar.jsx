import React, { useState, useEffect, useRef } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import NavItem from './NavItem';
import Hamburguer from './Hamburger';
import NavbarSkeleton from './NavbarSkeleton';
import useFetchAndLoad from '../../../hooks/useFetchAndLoad';
import getCMSContent from '../../../queries/getCMSContent';

const VARIANTS = {
  white: {
    background: 'transparent',
    mobileOpenBackground: 'white',
    text: 'black',
    mobileOpenText: 'black',
  },

  black: {
    background: 'transparent',
    mobileOpenBackground: 'black',
    text: 'white',
    mobileOpenText: 'black',
  },
};

function Navbar({ variant, className }) {
  const [mobileMenu, setToggleMobileMenu] = useState('hidden');
  const [headerBackground, setHeaderBackground] = useState(
    `bg-${VARIANTS[variant].background}`,
  );
  const [logoColor, setLogoColor] = useState(VARIANTS[variant].text);
  const [closeButton, setToggleCloseButton] = useState('');

  const body = useRef(document.querySelector('body'));
  const { callEndpoint, loading } = useFetchAndLoad();
  const [content, setContent] = useState({});

  useEffect(() => {
    const fetchLandingHeroSectionContent = async () => {
      const response = await callEndpoint(await getCMSContent('navbar'));
      if (response.status === 200) {
        setContent(response.data.data.attributes);
      }
    };
    fetchLandingHeroSectionContent();
  }, []);

  const toggleScroll = () => {
    if (body.current.style.overflow === 'hidden') {
      body.current.style.overflow = 'auto';
    } else {
      body.current.style.overflow = 'hidden';
    }
  };

  const toggleMenu = () => {
    // Toggle hamburger button
    setToggleCloseButton((prev) => {
      if (prev === 'toggle-btn') return '';
      return 'toggle-btn';
    });

    // Toggle mobile menu
    setToggleMobileMenu((prev) => {
      if (prev === 'hidden') return '';
      return 'hidden';
    });

    // Toggle header background
    setHeaderBackground((prev) => {
      if (prev === `bg-${VARIANTS[variant].background}`) return 'bg-white';
      return `bg-${VARIANTS[variant].background}`;
    });

    // Toggle logo color
    setLogoColor((prev) => {
      if (prev === VARIANTS[variant].text) {
        return VARIANTS[variant].mobileOpenText;
      }
      return VARIANTS[variant].text;
    });

    // Toggle all app scroll
    toggleScroll();
  };

  const handleMobileMenuOpen = () => {
    toggleMenu();
  };

  const resetMenuState = () => {
    setToggleMobileMenu('hidden');
    setToggleCloseButton('');
    setHeaderBackground(`bg-${VARIANTS[variant].background}`);
    setLogoColor(VARIANTS[variant].text);
    body.current.style.overflow = 'auto';
  };

  const resetMobileMenu = () => {
    resetMenuState();
  };

  window.addEventListener('resize', resetMobileMenu);

  if (loading) {
    return <NavbarSkeleton />;
  }

  return (
    <div
      className={`flex pt-[47px] items-baseline justify-between md:pt-[30px] lg:justify-start lg:pt-14 ${headerBackground} ${className}`}
    >
      {/* Desktop */}
      <header role="banner">
        <Link to="/" onClick={resetMobileMenu}>
          <h1
            className={`font-druk font-bold text-[27px] ml-4 mr-11 lg:ml-24 md:ml-10 text-${logoColor}`}
          >
            {content.title}
          </h1>
        </Link>
      </header>
      <nav role="navigation" className="hidden lg:block">
        <ul className="flex align-middle justify-center gap-10 pl-7">
          {content?.navbar_options?.data?.map((option) => (
            <NavItem
              key={option.id}
              to={option.attributes.to}
              label={option.attributes.label}
              className={` text-lg text-${VARIANTS[variant].text}`}
            />
          ))}
        </ul>
      </nav>

      {/* Mobile */}
      <Hamburguer
        onHamburgerClick={handleMobileMenuOpen}
        closeButton={closeButton}
      />
      <section
        className={`top-[88px] md:top-[70px] justify-content-center fixed z-50 w-full origin-top animate-open-menu flex-col bg-white ${mobileMenu}`}
      >
        <nav
          className="flex min-h-screen flex-col items-center"
          aria-label="mobile"
        >
          <ul className="text-left w-full">
            {content?.navbar_options?.data?.map((option) => (
              <NavItem
                key={option.id}
                to={option.attributes.to}
                label={option.attributes.label}
                className="mt-[50px] ml-[17px] md:ml-10 text-[25px]"
                onClick={resetMobileMenu}
              />
            ))}
          </ul>
        </nav>
      </section>
    </div>
  );
}

Navbar.propTypes = {
  className: PropTypes.string,
  variant: PropTypes.oneOf(['white', 'black']).isRequired,
};

Navbar.defaultProps = {
  className: '',
};

export default Navbar;
