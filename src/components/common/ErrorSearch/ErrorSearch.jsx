import React from 'react';
import ErrorSearchBackground from './ErrorSearchBackground';

function ErrorSearch() {
  return (
    <div className="flex flex-col gap-4 items-center justify-center h-full pt-20">
      <ErrorSearchBackground />
      <p className="font-syne text-lg max-w-sm text-center">
        ¡No hemos encontrado lo que buscas!
      </p>
    </div>
  );
}

export default ErrorSearch;
