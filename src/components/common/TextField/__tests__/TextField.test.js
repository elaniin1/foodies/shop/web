import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import TextField from '../TextField';

// text field input component props

// label: string,
// id: string.isRequired,
// placeholder: string,
// type: string.isRequired,
// labelStyle: string,
// inputStyle: string,
// required: bool,
// invalidMessage: string,
// className: string,

const MOCK_PROPS = {
  label: 'This is a label',
  id: 'mock-id',
  placeholder: 'This is a placeholder',
  type: 'text',
  labelStyle: 'mock-label-style',
  inputStyle: 'mock-input-style',
  required: true,
  invalidMessage: 'This is an invalid message',
  className: 'mock-class-name',
};

describe('TextField', () => {
  it('renders correctly', () => {
    render(<TextField {...MOCK_PROPS} />);

    expect(screen.getByTestId('TextField')).toBeInTheDocument();
  });

  it('renders with label', () => {
    render(<TextField {...MOCK_PROPS} />);

    expect(screen.getByTestId('TextField')).toHaveTextContent(MOCK_PROPS.label);
  });

  it('renders with className', () => {
    render(<TextField {...MOCK_PROPS} />);

    expect(screen.getByTestId('TextField')).toHaveClass(MOCK_PROPS.className);
  });

  it('can receive user input', () => {
    render(<TextField {...MOCK_PROPS} />);

    const mockInput = 'This is a mock input';
    const input = screen.getByTestId('TextField').querySelector('input');

    expect(input).toBeInTheDocument();

    fireEvent.change(input, { target: { value: mockInput } });

    expect(input.value).toBe(mockInput);
  });

  it("doesn't render invalid message when input is valid", () => {
    render(<TextField {...MOCK_PROPS} />);

    const mockInput = 'This is a mock input';
    const input = screen.getByTestId('TextField').querySelector('input');

    expect(input).toBeInTheDocument();

    fireEvent.change(input, { target: { value: mockInput } });

    expect(
      screen.queryByText(MOCK_PROPS.invalidMessage),
    ).not.toBeInTheDocument();

    expect(input.value).toBe(mockInput);

    expect(input.validity.valid).toBe(true);
  });

  it('renders invalid message when input is invalid', () => {
    render(<TextField {...MOCK_PROPS} />);

    const mockInput = '';
    const input = screen.getByTestId('TextField').querySelector('input');

    expect(input).toBeInTheDocument();

    fireEvent.invalid(input, { target: { value: mockInput } });

    expect(screen.getByTestId('TextField-invalid-message')).toBeInTheDocument();

    expect(input.validity.valid).toBe(false);
  });
});
