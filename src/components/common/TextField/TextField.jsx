import React, { useState } from 'react';
import PropTypes from 'prop-types';

function TextField({
  label,
  id,
  placeholder,
  type,
  labelStyle,
  inputStyle,
  required,
  invalidMessage,
  className,
}) {
  const [showInvalidMessage, setShowInvalidMessage] = useState('');

  return (
    <div
      data-testid="TextField"
      className={`flex-col text-white/100 ${className}`}
    >
      <label className={`items-start justify-start ${labelStyle}`} htmlFor={id}>
        <p className="max-w-fit font-openSans">{label}</p>
      </label>
      <input
        className={`block w-full bg-transparent border border-[#ffffff] rounded px-5 py-[14px] mt-[5px] ${inputStyle} `}
        type={type}
        id={id}
        name={id}
        placeholder={placeholder}
        onInvalid={(e) => {
          e.preventDefault();
          setShowInvalidMessage(true);
        }}
        onChange={() => {
          setShowInvalidMessage(false);
        }}
        required={required}
      />

      {required && showInvalidMessage ? (
        <p
          data-testid="TextField-invalid-message"
          className="text-red-700 font-semibold font-openSans text-sm"
        >
          {invalidMessage}
        </p>
      ) : null}
    </div>
  );
}

TextField.propTypes = {
  label: PropTypes.string,
  id: PropTypes.string.isRequired,
  placeholder: PropTypes.string,
  type: PropTypes.string.isRequired,
  labelStyle: PropTypes.string,
  inputStyle: PropTypes.string,
  required: PropTypes.bool,
  invalidMessage: PropTypes.string,
  className: PropTypes.string,
};

TextField.defaultProps = {
  label: '',
  placeholder: '',
  labelStyle: '',
  inputStyle: '',
  required: false,
  invalidMessage: '',
  className: '',
};

export default TextField;
