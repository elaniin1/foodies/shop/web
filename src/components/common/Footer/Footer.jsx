import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import AppStoreLogo from '../../../assets/img/app_store.png';
import GooglePlayLogo from '../../../assets/img/google_play.png';
import FooterSkeleton from './FooterSkeleton';
import useFetchAndLoad from '../../../hooks/useFetchAndLoad';
import getCMSContent from '../../../queries/getCMSContent';

function Footer() {
  const { callEndpoint, loading } = useFetchAndLoad();
  const [content, setContent] = useState({});

  useEffect(() => {
    const fetchLandingHeroSectionContent = async () => {
      const response = await callEndpoint(await getCMSContent('footer'));
      if (response.status === 200) {
        setContent(response.data.data.attributes);
      }
    };
    fetchLandingHeroSectionContent();
  }, []);

  if (loading) {
    return <FooterSkeleton />;
  }

  return (
    <footer
      className="px-4 pb-12 pt-36 bg-white
      md:px-6
      lg:px-14"
    >
      <div
        className="flex flex-wrap px-4 gap-5 items-center justify-center pb-7
        sm:justify-between
        md:px-14
        lg:px-24"
      >
        <span className="font-druk text-3xl text-[#7B7B7B]">
          {content.title}
        </span>
        <div className="flex gap-2">
          <img
            src={AppStoreLogo}
            alt="App Store Logo"
            className="w-1/2 h-auto -ml-1 sm:m-0"
          />
          <img
            src={GooglePlayLogo}
            alt="Google Play Logo"
            className="w-1/2 h-auto"
          />
        </div>
      </div>
      <div className="h-px w-full bg-[#FFC700]" />
      <ul
        className="flex flex-col items-center justify-center gap-11 pt-10 font-openSans text-[#00000066]
        sm:flex-row sm:flex-wrap
        md:px-14 md:justify-start
        lg:px-24
        "
      >
        {content?.footer_options?.data?.map((option) => (
          <li key={option.id}>
            <Link to={option.attributes.to}>{option.attributes.label}</Link>
          </li>
        ))}
      </ul>
    </footer>
  );
}

export default Footer;
