import React from 'react';
import HeroBackground from './HeroBackground';
import TextSkeleton from '../TextSkeleton/TextSkeleton';
import ImageSkeleton from '../ImageSkeleton/ImageSkeleton';

function HeroSectionSkeleton() {
  return (
    <>
      <TextSkeleton />
      <ImageSkeleton />
      <HeroBackground />
    </>
  );
}

export default HeroSectionSkeleton;
