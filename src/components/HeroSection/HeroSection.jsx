import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import Title from '../common/Title/Title';
import HeroSectionSkeleton from './HeroSectionSkeleton';
import HeroBackground from './HeroBackground';
import SectionLink from '../common/SectionLink/SectionLink';
import ArrowRight from '../Icons/ArrowRight';
import Text from '../common/Text/Text';
import useFetchAndLoad from '../../hooks/useFetchAndLoad';
import getCMSContent from '../../queries/getCMSContent';

const DEV_CSM_URL = import.meta.env.VITE_CMS_API;

function HeroSection({ id }) {
  const { callEndpoint, loading } = useFetchAndLoad();
  const [content, setContent] = useState({});

  useEffect(() => {
    const fetchLandingHeroSectionContent = async () => {
      const response = await callEndpoint(
        await getCMSContent('landing-hero-section'),
      );
      if (response.status === 200) {
        setContent(response.data.data.attributes);
      }
    };
    fetchLandingHeroSectionContent();
  }, []);

  if (loading) {
    return (
      <section
        id={id}
        className="flex flex-col-reverse ml-5 justify-between
        md:ml-[53px] md:flex-row md:items-center
        lg:ml-[102px] ld:flex-row lg:items-center gap-10"
      >
        <HeroSectionSkeleton />
      </section>
    );
  }

  return (
    <section
      id={id}
      className="flex flex-col-reverse ml-5 justify-between
       md:ml-[53px] md:flex-row md:items-center
       lg:ml-[102px] ld:flex-row lg:items-center"
    >
      <div className="mt-[64px] md:mt-[127px] lg:mt-[103px]">
        <div className="relative max-w-[343px] md:max-w-[335px] lg:max-w-[579px] -z-40">
          <Title className="text-[35px] md:text-[40px] lg:text-6xl">
            {content.title}
          </Title>
          <div
            className="bg-[#FFD600] w-[242px] h-[54px] absolute -rotate-[0.64deg] -bottom-[11px] -translate-x-[6px] -z-50
          md:w-[337px] md:-translate-x-[6px] md:-bottom-[11px]
          lg:w-[411px] lg:-bottom-[6px]"
          />
        </div>
        <Text
          className="mt-8 text-base max-w-[310px]
          md:mt-5 md:max-w-[280px]
          lg:mt-[21px] lg:text-lg lg:max-w-[470px]"
        >
          {content.text}
        </Text>
        <SectionLink
          to={content.toSectionRoute || ''}
          label={content.toSectionLabel || ''}
          className="text-[22px] mt-[27px]"
          rightIcon={<ArrowRight />}
        />
      </div>
      <div className="self-center">
        <img
          src={
            content?.image?.data?.attributes.url.includes('http')
              ? content?.image?.data?.attributes.url
              : `${DEV_CSM_URL}${content?.image?.data?.attributes.url}`
          }
          alt="regular hamburger"
          className="max-w-screen h-auto top-[121px] mt-[47px]
          md:mr-[30px] md:max-w-[312px] md:h-[246px]
          lg:h-[446px] lg:max-w-none lg:mt-20 xl:mr-24"
        />
        <HeroBackground />
      </div>
    </section>
  );
}

HeroSection.propTypes = {
  id: PropTypes.string,
};

HeroSection.defaultProps = {
  id: '',
};

export default HeroSection;
