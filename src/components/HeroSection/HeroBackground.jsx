import React from 'react';
import DesktopBackground from '../Backgrounds/HeroSectionBackgrounds/DesktopBackground';
import TabletBackground from '../Backgrounds/HeroSectionBackgrounds/TabletBackground';
import MobileBackground from '../Backgrounds/HeroSectionBackgrounds/MobileBackground';

function HeroBackground() {
  return (
    <>
      <DesktopBackground className="absolute top-0 right-0 -z-50 hidden lg:block" />
      <TabletBackground className="absolute top-0 right-0 -z-50 hidden md:block" />
      <MobileBackground className="absolute top-0 right-0 -z-50 md:hidden" />
    </>
  );
}

export default HeroBackground;
