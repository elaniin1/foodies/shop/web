import React, { useEffect } from 'react';
import { Routes, Route, useLocation } from 'react-router-dom';
import MenuPage from '../pages/MenuPage';
import HomePage from '../pages/HomePage';

function AppRouter() {
  const { pathname, hash, key } = useLocation();

  useEffect(() => {
    if (hash === '') {
      window.scrollTo(0, 0);
    } else {
      const id = hash.replace('#', '');
      const element = document.getElementById(id);
      if (element) {
        element.scrollIntoView();
      }
    }
  }, [pathname, hash, key]); // do this on route change

  return (
    <Routes>
      <Route path="/" element={<HomePage />} />
      <Route path="/menu" element={<MenuPage />} />
    </Routes>
  );
}

export default AppRouter;
