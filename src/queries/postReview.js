import axios from 'axios';
import loadAbort from '../utilities/loadAbortAxios.utility';

const API_URL = import.meta.env.VITE_FOODIES_API;

export const postReview = async (contactInfo) => {
  const controller = loadAbort();

  return {
    call: axios.post(
      `${API_URL}/forms/contact/submissions`,
      contactInfo,
      {
        headers: {
          'Content-Type': 'multipart/form-data',
        },
      },
      {
        signal: controller.signal,
      },
    ),
    controller,
  };
};

export default postReview;
