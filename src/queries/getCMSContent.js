import axios from 'axios';
import loadAbort from '../utilities/loadAbortAxios.utility';

const API_URL = import.meta.env.VITE_CMS_API;

export const getCMSContent = async (endpoint) => {
  const controller = loadAbort();

  return {
    call: axios.get(`${API_URL}/api/${endpoint}?populate=*`, {
      signal: controller.signal,
    }),
    controller,
  };
};

export default getCMSContent;
