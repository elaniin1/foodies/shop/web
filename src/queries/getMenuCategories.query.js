import axios from 'axios';
import loadAbort from '../utilities/loadAbortAxios.utility';

const API_URL = import.meta.env.VITE_FOODIES_API;

export const getMenuCategories = async () => {
  const controller = loadAbort();

  return {
    call: axios.get(
      `${API_URL}/categories`,

      {
        signal: controller.signal,
      },
    ),
    controller,
  };
};

export default getMenuCategories;
