import axios from 'axios';
import loadAbort from '../utilities/loadAbortAxios.utility';

const API_URL = import.meta.env.VITE_FOODIES_API;

export const getDishes = async (params) => {
  const controller = loadAbort();
  const queryParams = Object.keys(params)
    .map((key) => {
      if (key === 'categories') {
        return params[key]
          .map((category) => {
            if (category === '') return '';
            return `categories[]=${category}`;
          })
          .join('&');
      }
      return `${key}=${params[key]}`;
    })
    .join('&');

  return {
    call: axios.get(
      `${API_URL}/dishes?${queryParams}`,

      {
        signal: controller.signal,
      },
    ),
    controller,
  };
};

export default getDishes;
