/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ['./index.html', './src/**/*.{js,ts,jsx,tsx}'],
  theme: {
    extend: {
      fontFamily: {
        druk: ['DrukTextWide', 'sans-serif'],
        syne: ['Syne', 'sans-serif'],
        openSans: ['"Open Sans"', 'sans-serif'],
        roboto: ['Roboto', 'sans-serif'],
      },

      keyframes: {
        'open-menu': {
          '0%': { transform: 'scaleY(0)' },
          '100%': { transform: 'scaleY(1)' },
        },
      },

      animation: {
        'open-menu': 'open-menu 0.25s ease-in-out forwards',
      },

      screens: {
        xs: '320px',
      },
    },
  },
  plugins: [],
};
